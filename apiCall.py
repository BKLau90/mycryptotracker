import requests
import requests_cache
import json
from requests import api

requests_cache.install_cache('api_cache', backend='sqlite', expire_after=180)

def get_top_5():
        url = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=5&page=1&sparkline=false'
        try:
           response = requests.get(url, timeout=60)
           data = json.loads(response.text)
           return data
        except json.decoder.JSONDecodeError:
        # wait 5 seconds and try again. This would require you to put your try block 
        # into it's own function so you can more easily call it here in the exception
             print("Error querying coingecko API")
             
