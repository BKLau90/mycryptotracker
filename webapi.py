from flask import Flask, render_template
from apiCall import get_top_5
import threading
app = Flask(__name__)
app.config["DEBUG"] = True

@app.route('/', methods=['GET'])
def display_cryptoprice():
    results = get_top_5()
    for result in results:
        result['current_price'] = '$ ' + "{:.2f}".format(result['current_price'])
    return render_template('index.html', **locals())

if __name__ == "__main__":
    app.run(host='0.0.0.0')
